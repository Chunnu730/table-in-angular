import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Student } from './student';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const students = [
      { id: 11, roll: 17501, reg: 17105129001, name: 'Dr Nice', dues: 500 },
      { id: 12, roll: 17502, reg: 17105129002, name: 'Narco', dues: 1000 },
      { id: 13, roll: 17503, reg: 17105129003, name: 'Bombasto', dues: 1500 },
      { id: 14, roll: 17504, reg: 17105129004, name: 'Celeritas', dues: 2000 },
      { id: 15, roll: 17505, reg: 17105129005, name: 'Magneta', dues: 2300 },
      { id: 16, roll: 17506, reg: 17105129006, name: 'RubberMan', dues: 1300 },
      { id: 17, roll: 17507, reg: 17105129007, name: 'Dynama', dues: 1400 },
      { id: 18, roll: 17508, reg: 17105129008, name: 'Dr IQ', dues: 1200 },
      { id: 19, roll: 17509, reg: 17105129009, name: 'Magma', dues: 900 },
      { id: 20, roll: 17510, reg: 17105129010, name: 'Tornado', dues: 800 },
      { id: 21, roll: 17511, reg: 17105129056, name: 'RubberMan', dues: 1900 },
      { id: 22, roll: 17527, reg: 17105129027, name: 'Dynama', dues: 1400 },
      { id: 23, roll: 17518, reg: 17105129018, name: 'Dr IQ', dues: 1200 },
      { id: 24, roll: 17529, reg: 17105129039, name: 'Magma', dues: 1900 },
      { id: 25, roll: 17519, reg: 17105129019, name: 'Tornado', dues: 8100 },
    ];
    return { students };
  }
  genId(students: Student[]): number {
    return students.length > 0
      ? Math.max(...students.map((student) => student.id)) + 1
      : 11;
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(private studentService: StudentService) {}

  ngOnInit(): void {}

  search(term: string): void {
    this.studentService.search(term);
  }
}

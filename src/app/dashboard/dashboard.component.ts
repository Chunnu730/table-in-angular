import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  constructor(public studentService: StudentService) {}

  ngOnInit(): void {}
  searchBox: any ='';

  condition(term: string):void{
    this.searchBox = term;
  }
}
export interface Student{
    id:number,
    roll: number,
    reg:number,
    name: string,
    dues: number
}
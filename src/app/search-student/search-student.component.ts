import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StudentService } from '../student.service';
import { Student } from '../student';
import { Observable, map, tap } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-search-student',
  templateUrl: './search-student.component.html',
  styleUrls: ['./search-student.component.css'],
})
export class SearchStudentComponent implements OnInit {
  constructor(private studentService: StudentService) {}

  @Output() updateDataEvent = new EventEmitter<any>();
  students$!: Observable<Student[]>;
  searchBox: string = '';

  ngOnInit(): void {
    this.getStudents();
    this.students$ = this.studentService.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        this.updateDataEvent.emit(term);
        this.searchBox = term;
        return this.studentService.searchStudent(term);
      })
    );
  }

  students: Student[] = [];

  getStudents(): any {
    this.studentService
      .getStudents()
      .subscribe((students) => (this.students = students));
  }

  deleteStudent(student: Student): void {
    this.students$.pipe(
      map((element) => element.filter((item) => item !== student))
    );
    this.studentService.deleteStudent(student.id).subscribe();
  }

  sortName(): void {
    this.students$.pipe(
      map((element) =>
        element.sort((a, b) => {
          const nameA = a.name.toUpperCase();
          const nameB = b.name.toUpperCase();
          if (nameA > nameB) {
            return 1;
          } else if (nameA < nameB) {
            return -1;
          } else {
            return 0;
          }
        })
      )
    );
  }

  sortRoll(): void {
    this.students$.pipe(
      tap((element) => element.sort((a, b) => (a > b ? 1 : -1)))
    ).subscribe(data => console.log("data"+data))
  }
}

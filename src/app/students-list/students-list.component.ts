import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { Student } from '../student';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css'],
})
export class StudentsListComponent implements OnInit {
  constructor(private studentService: StudentService) {}

  ngOnInit(): void {
    this.getStudents();
  }
  students: Student[] = [];

  getStudents(): any {
    this.studentService
      .getStudents()
      .subscribe((students) => (this.students = students));
  }

  deleteStudent(student: Student): void {
    this.students = this.students.filter((h) => h !== student);
    this.studentService.deleteStudent(student.id).subscribe();
  }

  sortName(): void {
    this.students.sort((a, b) => {
      const nameA = a.name.toUpperCase();
      const nameB = b.name.toUpperCase();
      if (nameA > nameB) {
        return 1;
      } else if (nameA < nameB) {
        return -1;
      } else {
        return 0;
      }
    });
  }

  sortRoll(): void{
    this.students.sort((a, b) => a.roll - b.roll);
  }
}

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { StudentService } from '../student.service';
import { Student } from '../student';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css'],
})
export class AddStudentComponent implements OnInit {
  constructor(
    private studentService: StudentService,
    private location: Location
  ) {}

  students: Student[] = [];
  display: boolean = false;
  checkUniqueRoll?: number;

  addStudent = new FormGroup({
    roll: new FormControl('', [Validators.required, this.checkRoll.bind(this)]),
    reg: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    dues: new FormControl('', [Validators.required]),
  });

  get roll() {
    return this.addStudent.get('roll');
  }

  get reg() {
    return this.addStudent.get('reg');
  }

  get name() {
    return this.addStudent.get('name');
  }

  get dues() {
    return this.addStudent.get('dues');
  }

  ngOnInit(): void {
    this.getStudents();
  }

  getStudents(): void {
    this.studentService
      .getStudents()
      .subscribe((students) => (this.students = students));
  }

  getBack(): void {
    this.location.back();
  }

  checkRoll(control: FormControl) {
    if (this.students.find((element) => element.roll === control.value)) {
      this.display = true;
      return {'invalidRoll': true };
    } else {
      this.display = false;
      return null;
    }
  }

  add(student: any): void {
    if (!student) {
      return;
    }
    if (student) {
      this.studentService
        .addStudent(this.addStudent.value)
        .subscribe(() => this.getBack());
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from './student';
import { Observable, of, Subject } from 'rxjs';
import { catchError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  constructor(private http: HttpClient) {}

  private studentsUrl = 'api/students';
  public searchTerms = new Subject<string>();
  searchBox: string = '';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  addStudent(student: Student): Observable<Student> {
    return this.http
      .post<Student>(this.studentsUrl, student, this.httpOptions)
      .pipe(catchError(this.handleError<Student>('addHero')));
  }

  getStudents(): Observable<Student[]> {
    return this.http
      .get<Student[]>(this.studentsUrl)
      .pipe(catchError(this.handleError<Student[]>('getStudent', [])));
  }

  getStudent(id: number): Observable<Student> {
    const url = `${this.studentsUrl}/${id}`;
    return this.http
      .get<Student>(url)
      .pipe(catchError(this.handleError<Student>(`getStudent id=${id}`)));
  }

  updateStudent(student: Student): Observable<any> {
    return this.http
      .put(this.studentsUrl, student, this.httpOptions)
      .pipe(catchError(this.handleError<any>('updateHero')));
  }

  deleteStudent(id: number): Observable<Student> {
    const url = `${this.studentsUrl}/${id}`;
    return this.http
      .delete<Student>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Student>('deleteHero')));
  }

  search(term: string): void {
    this.searchBox = term;
    this.searchTerms.next(term);
  }

  searchStudent(term: string): Observable<Student[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http
      .get<Student[]>(`${this.studentsUrl}/?name=${term}`)
      .pipe(catchError(this.handleError<Student[]>('searchStudents', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
